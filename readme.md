

# Cuke-API Test Harness

-------------------
# What is it?

This project comprises a scalable, dynamic, configurable and portable test harness, the purpose of which is the automation of API test cases related to the Emergent Payments, Inc. V3 API.

* Uses the [Cucumber BDD](https://cucumber.io/) collaboration tool
* Glue code used to execute Cucumber step-definitions is written in [Java](https://en.wikipedia.org/wiki/Java_(programming_language))
* The [Rest-Assured](https://github.com/jayway/rest-assured) library is used to interact with the API for testing purposes

# Getting Started

* This is a Maven project. The included pom.xml includes all dependencies required for the project to execute correctly
* From the bottom up:
    * Test cases are contained in the Cucumber \*.feature files
    * Each feature file may contain several scenarios or scenario outlines
    * Cucumber uses a set of @ tags to give greater control when executing the feature
    * This project contains tags to specify the type of feature, or API sepcific tags (like @INTERNAL or @PUBLIC) to drive the JUnit runners
    * A JUnit class has been established to drive suite-based execution, based on the tagging process mentioned above
* The Hooks class establishes general global variables that are used throughout:
    * The base application url is establihed in the api_automation.properties file
    * These variables can be overridden by a system property parameter set (for example) in a run configuration or in CI

* How to run tests:
    * Tests can be run as either a single feature file, or as a single JUnit suite (specified any one of the classes contained in unit_runners)
    * Simply select "run as" on any feature file, or JUnit suite

# Who do I talk to?

* This repository is maintained by Ryan Bedino:
    * Email: rlb_tester@icloud.com
* For more information, please reach out to Ryan, or alternately contact:
    * No one. There is no one else.