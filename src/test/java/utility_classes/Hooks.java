package utility_classes;

import cucumber.api.java.Before;
import org.junit.BeforeClass;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Hooks {
    public static String baseUrl;

    @Before
    public void setUp() throws Exception {
        String properties = "src/test/java/utility_classes/api_automation.properties";

        InputStream inGlobal = new FileInputStream(properties);
        Properties propGlobal = new Properties();
        propGlobal.load(inGlobal);

        baseUrl = propGlobal.getProperty("baseUrl");
        baseUrl = System.getProperty("base.url", baseUrl);

        inGlobal.close();
    }
}
