package unit_runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = {"pretty", "json:out/cucumber.json", "html:reports/API_ALL_EXECUTIONS_REPORT.json"},
        features = {"src/test/features"},
        glue = {"step_definitions"},
        tags = {"@api_test"})

public class CukeRunner_0001_API_Unit {

}
