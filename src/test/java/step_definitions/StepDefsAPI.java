package step_definitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang.StringUtils;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.config.RedirectConfig.redirectConfig;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import static utility_classes.Hooks.baseUrl;

public class StepDefsAPI {
    private Response response;
    private ValidatableResponse json;

    private String requestPath;
    private String errorMessage;
    private int statusCode;
    private String productId;
    private String jsonAsString;

    private String getRequestPath(String endpointName) throws UnsupportedEncodingException {
        this.requestPath = baseUrl + endpointName + "/";
        return requestPath;
    }

    @Given("^the '(.+)' endpoint$")
    public void set_endpoint(String endpointName) throws UnsupportedEncodingException {
        requestPath = getRequestPath(endpointName);
    }

    @When("^the endpoint is accessed using '(.+)'$")
    public void access_endpoint(String productId) throws Throwable {
        this.productId = productId;
        if (productId.contains("no")) {
            productId = "";
        }
        requestPath = requestPath + productId;
        response = given().when().get(requestPath);
        jsonAsString = response.asString();
    }

    @Then("^the status should be '(.+)'$")
    public void validate_resp_status(int statusCode) {
        this.statusCode = statusCode;
        this.json = response.then().statusCode(statusCode);
    }

    @And("response includes the following$")
    public void response_equals(Map<String,String> responseFields){
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            if(StringUtils.isNumeric(field.getValue())){
                json.body(field.getKey(), equalTo(Integer.parseInt(field.getValue())));
            }
            else{
                json.body(field.getKey(), equalTo(field.getValue()));
            }
        }
    }

    @Then("^the response body should contain the expected data$")
    public void validate_resp_body() throws Exception {
        ArrayList<Map<String,?>> jsonAsArrayList = from(jsonAsString).get("");
        assertThat(jsonAsArrayList.size(), equalTo(4));
    }

    @Then("^the user receives an error message$")
    public void response_error_message() throws Exception {
        switch (statusCode) {
            case 404:
                errorMessage = "No HTTP resource was found that matches the request URI 'http://pltestautomationsample.azurewebsites.net/api/bad_name/"
                        + productId + "'.";
                break;
            case 500:
                errorMessage = "An error has occurred.";
            default:
                break;
        }
        json.body("Message", equalTo(errorMessage));
    }
}