#@Ignore
@api_test
Feature: API - Tester

  Scenario: GET /product API Test - FIRST Object
    Given the 'product' endpoint
    When the endpoint is accessed using '0'
    Then the status should be '200'
    And response includes the following
      | Id  | 0 |
      | Name  | Red Truck |
      | Description  | A red truck |

  Scenario: GET /product API Test  - SECOND Object
    Given the 'product' endpoint
    When the endpoint is accessed using '1'
    Then the status should be '200'
    And response includes the following
      | Id  | 1 |
      | Name  | Blue Car |
      | Description  | A blue car |

  Scenario: GET /product API Test  - THIRD Object
    Given the 'product' endpoint
    When the endpoint is accessed using '2'
    Then the status should be '200'
    And response includes the following
      | Id  | 2 |
      | Name  | Toaster |
      | Description  | It doesn't drive, but it toasts |

  Scenario: GET /product API Test  - FOURTH Object
    Given the 'product' endpoint
    When the endpoint is accessed using '3'
    Then the status should be '200'
    And response includes the following
      | Id  | 3 |
      | Name  | Waffle iron |
      | Description  | It doesn't toast, but makes delicious waffles |

  Scenario: GET /product API Test - all / no product specified
    Given the 'product' endpoint
    When the endpoint is accessed using 'no_id'
    Then the status should be '200'
    And the response body should contain the expected data

  Scenario: GET /product API Test - using an out-of-range product id
    Given the 'product' endpoint
    When the endpoint is accessed using '4'
    Then the status should be '500'
    And the user receives an error message

  Scenario: GET /product API Test - using a bad endpoint name
    Given the 'bad_name' endpoint
    When the endpoint is accessed using '1'
    Then the status should be '404'
    And the user receives an error message




